// Package weather provides functionality for getting weather forecasts.
package weather

// CurrentCondition stores a string representation of the current weather conditions.
var CurrentCondition string

// CurrentLocation stores a string representation of the current location.
var CurrentLocation string

// Forecast takes two string arguments and returns a string representation of the current weather forecast in the given location.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
