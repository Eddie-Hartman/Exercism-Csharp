class SqueakyClean {
    static String clean(String identifier) {
        String upper = kebabToUpper(identifier);

        return upper.replace(' ', '_').replaceAll("[\0\r\u007F]", "CTRL").replaceAll("[^\\p{L}_]", "")
            .replaceAll("[\u03b1-\u03c9]", "");
    }

    private static String kebabToUpper(String identifier){
        StringBuilder result = new StringBuilder(identifier.length());
        boolean uppercaseNext = false;

        for (char ch : identifier.toCharArray()) {
            if (ch == '-') {
                uppercaseNext = true;
            } else {
                if (uppercaseNext && Character.isLowerCase(ch)) {
                    ch = Character.toUpperCase(ch);
                }
                result.append(ch);
                uppercaseNext = false;
            }
        }

        return result.toString();
    }
}
