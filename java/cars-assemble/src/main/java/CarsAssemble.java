public class CarsAssemble {

    public double productionRatePerHour(int speed) {
        return getSuccessRate(speed) * 221 * speed;
    }

    public int workingItemsPerMinute(int speed) {
        return (int)(productionRatePerHour(speed) / 60);
    }

    private double getSuccessRate(int speed){
        double successRate;
        if(speed < 5){
            successRate = 1;
        }
        else if(speed < 9){
            successRate = .9;
        }
        else if(speed == 9){
            successRate = .8;
        }
        else{
            successRate = .77;
        }
        return successRate;
    }
}
