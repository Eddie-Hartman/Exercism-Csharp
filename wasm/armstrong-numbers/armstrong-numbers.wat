(module
  ;; returns 1 if armstrong number, 0 otherwise
  (func (export "isArmstrongNumber") (param $candidate i32) (result i32)
    (local $sum i32)            ;; Sum of powers of digits
    (local $n i32)              ;; Copy of the candidate number for manipulation
    (local $digit i32)          ;; Individual digit
    (local $numDigits i32)      ;; Number of digits
    (local $pow i32)            ;; Power calculation
    (local $counter i32)        ;; Counter for power calculation loop

    ;; Initialize variables
    (local.set $sum (i32.const 0))
    (local.set $n (local.get $candidate))
    (local.set $numDigits (i32.const 0))

    ;; Calculate the number of digits
    (block
      (loop
        (br_if 1 (i32.eqz (local.get $n)))
        (local.set $n (i32.div_u (local.get $n) (i32.const 10)))
        (local.set $numDigits (i32.add (local.get $numDigits) (i32.const 1)))
        (br 0)
      )
    )

    ;; Reset $n to the candidate number
    (local.set $n (local.get $candidate))

    ;; Calculate the sum of digits each raised to the power of $numDigits
    (block
      (loop
        (br_if 1 (i32.eqz (local.get $n)))
        (local.set $digit (i32.rem_u (local.get $n) (i32.const 10)))
        (local.set $n (i32.div_u (local.get $n) (i32.const 10)))
        (local.set $pow (i32.const 1))
        (local.set $counter (local.get $numDigits))

        ;; Calculate digit ^ numDigits
        (block
          (loop
            (local.set $pow (i32.mul (local.get $pow) (local.get $digit)))
            (local.set $counter (i32.sub (local.get $counter) (i32.const 1)))
            (br_if 1 (i32.eqz (local.get $counter)))
            (br 0)
          )
        )
        (local.set $sum (i32.add (local.get $sum) (local.get $pow)))
        (br 0)
      )
    )

    ;; Compare sum and candidate number
    (i32.eq (local.get $sum) (local.get $candidate))
  )
)
