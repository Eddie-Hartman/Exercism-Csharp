(module
  (func (export "score") (param $x f32) (param $y f32) (result i32) (local $r f32)
    ;; Store the square of $x in a local variable
    local.get $x
    local.get $x
    f32.mul
    local.set $r

    ;; Add the square of $y to the local variable
    local.get $r
    local.get $y
    local.get $y
    f32.mul
    f32.add
    local.set $r

    ;; Take the square root of the sum to get the distance
    local.get $r
    f32.sqrt
    local.tee $r

    ;; Compare the distance with 10, return 0 if greater
    f32.const 10.0
    f32.gt
    if
      i32.const 0
      return
    end

    ;; Compare the distance with 5, return 1 if greater
    local.get $r
    f32.const 5.0
    f32.gt
    if
      i32.const 1
      return
    end

    ;; Compare the distance with 1, return 5 if greater
    local.get $r
    f32.const 1.0
    f32.gt
    if
      i32.const 5
      return
    end

    ;; If none of the above conditions are met, return 10
    i32.const 10
  )
)
