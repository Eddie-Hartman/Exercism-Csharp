class Lasagna
{
    const int MINUTES_IN_OVEN = 40;
    const int LAYER_MULTIPLIER = 2;
    public int ExpectedMinutesInOven() => MINUTES_IN_OVEN;

    public int RemainingMinutesInOven(int alreadyCooked) => MINUTES_IN_OVEN - alreadyCooked;

    public int PreparationTimeInMinutes(int layers) =>  layers * LAYER_MULTIPLIER;

    public int ElapsedTimeInMinutes(int layers, int alreadyCooked) => layers * LAYER_MULTIPLIER + alreadyCooked;
}
