using System;
using System.Globalization;
using System.Runtime.InteropServices;

public enum Location
{
    NewYork,
    London,
    Paris
}

public enum AlertLevel
{
    Early,
    Standard,
    Late
}

public static class Appointment
{
    public static DateTime ShowLocalTime(DateTime dtUtc) => dtUtc.ToLocalTime();

    public static DateTime Schedule(string appointmentDateDescription, Location location)
    {
        DateTime dateTime = DateTime.Parse(appointmentDateDescription);

        TimeZoneInfo timeZoneInfo = GetTimeZoneInfo(location);
        dateTime = ConvertToLocal(timeZoneInfo, dateTime);

        return dateTime.ToUniversalTime();
    }

    public static DateTime GetAlertTime(DateTime appointment, AlertLevel alertLevel) =>
        alertLevel switch
        {
            AlertLevel.Early => appointment.AddDays(-1),
            AlertLevel.Standard => appointment.AddHours(-1).AddMinutes(-45),
            _ => appointment.AddMinutes(-30),
        };

    public static bool HasDaylightSavingChanged(DateTime dt, Location location)
    {
        //Return true if daylight savings has become active in the last 7 days.
        TimeZoneInfo timeZoneInfo = GetTimeZoneInfo(location);
        dt = ConvertToLocal(timeZoneInfo, dt);
        bool original = timeZoneInfo.IsDaylightSavingTime(dt);
        dt = dt.AddDays(-7);
        return original != timeZoneInfo.IsDaylightSavingTime(dt);
    }

    public static DateTime NormalizeDateTime(string dtStr, Location location)
    {
        CultureInfo culture = CultureInfo.CreateSpecificCulture(location switch
        {
            Location.NewYork => "en-US",
            Location.London => "en-GB",
            Location.Paris => "fr-FR",
            _ => throw new ArgumentException()
        });
        return DateTime.TryParse(dtStr, culture, DateTimeStyles.None, out DateTime dateTime) ? dateTime : new DateTime();
    }

    private static DateTime ConvertToLocal(TimeZoneInfo timeZoneInfo, DateTime dateTime) =>
        TimeZoneInfo.ConvertTime(dateTime, timeZoneInfo, TimeZoneInfo.Local);

    private static TimeZoneInfo GetTimeZoneInfo(Location location)
    {
        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        return TimeZoneInfo.FindSystemTimeZoneById(location switch
        {
            Location.NewYork => isWindows ? "Eastern Standard Time" : "America/New_York",
            Location.London => isWindows ? "GMT Standard Time" : "Europe/London",
            _ => isWindows ? "W. Europe Standard Time" : "Europe/Paris",
        });
    }
}
