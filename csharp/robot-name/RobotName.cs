using System;
using System.Collections.Generic;
using System.Text;

public class Robot
{
    private static readonly HashSet<string> names = new HashSet<string>();
    private string _name = null;

    public string Name => _name ??= newName();

    public void Reset()
    {
        names.Remove(_name);
        _name = null;
    }

    private string newName()
    {
        StringBuilder stringBuilder = new StringBuilder(5);

        Random rand = new Random();

        bool unique = false;

        while (!unique)
        {
            stringBuilder.Clear();
            stringBuilder.Append(randomLetter(rand));
            stringBuilder.Append(randomLetter(rand));
            stringBuilder.Append(rand.Next(10));
            stringBuilder.Append(rand.Next(10));
            stringBuilder.Append(rand.Next(10));
            unique = names.Add(stringBuilder.ToString());
        }

        return stringBuilder.ToString();
    }

    /// <summary>
    /// Returns a random capital letter.
    /// </summary>
    /// <param name="rand">Random object to use to generate the random letter.</param>
    /// <returns>Random capital letter.</returns>
    private char randomLetter(Random rand) => (char)rand.Next('A', 'Z' + 1);
}