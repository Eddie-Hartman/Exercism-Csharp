using System;

abstract class Character
{
    protected bool vulnerable = false;
    private readonly string characterType;

    protected Character(string characterType) => this.characterType = characterType;

    public abstract int DamagePoints(Character target);

    public virtual bool Vulnerable() => vulnerable;

    public override string ToString() => $"Character is a {characterType}";
}

class Warrior : Character
{
    public Warrior() : base("Warrior")
    {
    }

    public override int DamagePoints(Character target) => target.Vulnerable() ? 10 : 6;
}

class Wizard : Character
{
    private bool spellReady = false;
    public Wizard() : base("Wizard")
    {
    }

    public override int DamagePoints(Character target) => spellReady ? 12 : 3;

    public void PrepareSpell() => spellReady = true;

    public override bool Vulnerable() => !spellReady;
}
