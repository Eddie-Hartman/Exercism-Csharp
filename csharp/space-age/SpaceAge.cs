public class SpaceAge
{
    private readonly double onEarth;

    private const double EarthOrbit = 31557600, MercuryOrbit = 0.2408467, VenusOrbit = 0.61519726,
        MarsOrbit = 1.8808158, JupiterOrbit = 11.862615, SaturnOrbit = 29.447498, UranusOrbit = 84.016846,
        NeptuneOrbit = 164.79132;

    public SpaceAge(int seconds) => onEarth = seconds / EarthOrbit;

    public double OnEarth() => onEarth;

    public double OnMercury() => onEarth / MercuryOrbit;

    public double OnVenus() => onEarth / VenusOrbit;

    public double OnMars() => onEarth / MarsOrbit;

    public double OnJupiter() => onEarth / JupiterOrbit;

    public double OnSaturn() => onEarth / SaturnOrbit;

    public double OnUranus() => onEarth / UranusOrbit;

    public double OnNeptune() => onEarth / NeptuneOrbit;
}