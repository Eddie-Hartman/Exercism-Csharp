﻿using System;
using System.Text;

public static class Identifier
{
    public static string Clean(string identifier)
    {
        StringBuilder stringBuilder = new StringBuilder(identifier);

        stringBuilder.Replace("\0", "CTRL");

        int index = 0;
        while(index < stringBuilder.Length)
        {
            char c = stringBuilder[index];

            if (c == ' ')
                stringBuilder[index] = '_';
            else if (c == '-')
            {
                if (index < stringBuilder.Length - 1)
                {
                    stringBuilder[index + 1] = Char.ToUpper(stringBuilder[index + 1]);
                    stringBuilder.Remove(index, 1);
                    index--;
                }
            }
            else if (!char.IsLetter(c) || (c >= 'α' && c <= 'ω'))
            {
                stringBuilder.Remove(index, 1);
                index--;
            }
            index++;
        }

        return stringBuilder.ToString();
    }
}
