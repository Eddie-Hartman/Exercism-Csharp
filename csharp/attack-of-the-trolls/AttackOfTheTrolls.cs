using System;

public enum AccountType
{
    Guest = Permission.Read,
    User = Permission.Write | Permission.Read,
    Moderator = Permission.All
}

[Flags]
public enum Permission
{
    Read = 1,
    Write = 2,
    Delete = 4,
    All = 7,
    None = 0
}

static class Permissions
{
    public static Permission Default(AccountType accountType)
    {
        return Enum.IsDefined(typeof(AccountType), accountType) ? (Permission)accountType: Permission.None;
    }

    public static Permission Grant(Permission current, Permission grant)
    {
        return current | grant;
    }

    public static Permission Revoke(Permission current, Permission revoke)
    {
        return current & ~revoke;
    }

    public static bool Check(Permission current, Permission check)
    {
        return (current & check) == check;
    }
}
