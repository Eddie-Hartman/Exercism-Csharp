using System;

public static class Gigasecond
{
    /// <summary>
    /// Return a DateTime one gigasecond later than the given DateTime.
    /// </summary>
    /// <param name="moment"></param>
    /// <returns></returns>
    public static DateTime Add(DateTime moment) => moment.AddSeconds(1000000000);
}