using System;

class WeighingMachine
{
    public int Precision {get;}

    private double weight;
    public double Weight 
    {
        get { return weight; }
        set 
        {
            if(value < 0)
                throw new ArgumentOutOfRangeException();
            else
                weight = value;
        }
    }

    public string DisplayWeight
    {
        get { return $"{String.Format($"{{0:F{Precision}}}",Weight - TareAdjustment)} kg"; }
    }

    public double TareAdjustment {get; set;} = 5;

    public WeighingMachine(int precision)
    {
        Precision = precision;
    }
}
