using System;
using System.Text;

public class SimpleCipher
{
    public SimpleCipher()
    {
        StringBuilder builder = new();
        Random rnd = new Random();
        for (int i = 0; i < 100; i++)
        {
            builder.Append((char)(rnd.Next(0, 26) + 97));
        }
        Key = builder.ToString();
    }

    public SimpleCipher(string key) => Key = key;

    public string Key { get; }

    public string Encode(string plaintext)
    {
        StringBuilder builder = new();
        for(int i = 0; i < plaintext.Length; i++)
        {
            builder.Append((char)((plaintext[i] - 97 + Key[i % Key.Length] - 97) % 26 + 97));
        }
        return builder.ToString();
    }

    public string Decode(string ciphertext)
    {
        StringBuilder builder = new();
        for (int i = 0; i < ciphertext.Length; i++)
        {
            builder.Append((char)((ciphertext[i] - 97 - Key[i % Key.Length] + 123) % 26 + 97));
        }
        return builder.ToString();
    }
}