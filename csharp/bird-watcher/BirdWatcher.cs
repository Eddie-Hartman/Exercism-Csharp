using System;

class BirdCount
{
    private int[] birdsPerDay;

    public BirdCount(int[] birdsPerDay)
    {
        this.birdsPerDay = birdsPerDay;
    }

    public static int[] LastWeek() => new int[] {0, 2, 5, 3, 7, 8, 4};

    public int Today() => birdsPerDay[^1];

    public void IncrementTodaysCount() => birdsPerDay[^1]++;

    public bool HasDayWithoutBirds()
    {
        int index = 0;
        while(index < birdsPerDay.Length)
        {
            if (birdsPerDay[index] == 0)
                return true;
            index++;
        }
        return false;
    }

    public int CountForFirstDays(int numberOfDays)
    {
        int index = 0, total = 0;
        
        while(index < numberOfDays)
        {
            total += birdsPerDay[index];
            index++;
        }
        return total;
    }

    public int BusyDays()
    {
        int index = 0, total = 0;

        while (index < birdsPerDay.Length)
        {
            if(birdsPerDay[index] >= 5)
                total += 1;
            index++;
        }
        return total;
    }
}
