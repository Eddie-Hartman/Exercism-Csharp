﻿using System;
using System.Linq;

public static class Pangram
{
    public static bool IsPangram(string input)
    {
        if (input == null)
            throw new ArgumentException("Null not supported.");

        input = input.ToUpperInvariant();
        bool[] includedCharacters = new bool[26];

        char[] charArray = input.ToCharArray();

        for(int i = 0; i < charArray.Length; i++)
        {
            //The current character in the string to look at.
            int current = (int)charArray[i];
            //65 is 'A', 90 is 'Z'
            if(65 <= current && current <= 90)
            {
                current %= 65;
                includedCharacters[current] = true;
            }
        }
        return includedCharacters.All(x => x);
    }
}
