using System;

public static class PlayAnalyzer
{
    public static string AnalyzeOnField(int shirtNum) =>
        shirtNum switch
        {
            1 => "goalie",
            2 => "left back",
            3 or 4 => "center back",
            5 => "right back",
            6 or 7 or 8 => "midfielder",
            9 => "left wing",
            10 => "striker",
            11 => "right wing",
            _ => throw new ArgumentOutOfRangeException()
        };

    public static string AnalyzeOffField(object report) =>
        report switch
        {
            _ when report is string => report?.ToString() ?? "",
            _ when report is int => $"There are {report} supporters at the match.",
            _ when report is Injury => $"Oh no! {((Injury)report).GetDescription()} Medics are on the field.",
            _ when report is Incident => ((Incident)report).GetDescription(),
            _ when report is Manager =>
                ((Manager)report).Club == null ? ((Manager)report).Name : $"{((Manager)report).Name} ({((Manager)report).Club})",
            _ => throw new ArgumentException(),
        };
}
