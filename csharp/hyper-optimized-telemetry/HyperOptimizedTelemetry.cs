using System;
using System.Collections.Generic;

public static class TelemetryBuffer
{
    public static byte[] ToBuffer(long reading)
    {
        sbyte firstByte = reading switch
        {
            _ when reading >= 4_294_967_296 => -8,
            _ when reading >= 2_147_483_648 => 4,
            _ when reading >= 65_536 => -4,
            _ when reading >= 0 => 2,
            _ when reading >= -32_768 => -2,
            _ when reading >= -2_147_483_648 => -4,
            _ => -8
        };
        byte[] buffer = new byte[9];
        buffer[0] = (byte)firstByte;
        byte[] readingBytes = BitConverter.GetBytes(reading);
        for(int i = 0; i < Math.Abs(firstByte); i++)
        {
            buffer[i+1] = readingBytes[i];
        }

        return buffer;
    }

    public static long FromBuffer(byte[] buffer)
    {
        sbyte firstByte = (sbyte)buffer[0];

        if (!new List<sbyte>{ -8, -4, -2, 2, 4 }.Contains(firstByte))
            return 0;

        int temp = Math.Abs(firstByte) + 1;
        Span<byte> bytes = buffer.AsSpan()[1..temp];

        return firstByte switch
        {
            2 => BitConverter.ToUInt16(bytes),
            4 => BitConverter.ToUInt32(bytes),
            -2 => BitConverter.ToInt16(bytes),
            -4 => BitConverter.ToInt32(bytes),
            _ => BitConverter.ToInt64(bytes),
        };
    }
}
