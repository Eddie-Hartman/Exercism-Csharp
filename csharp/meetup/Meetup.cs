using System;

public enum Schedule
{
    Teenth,
    First,
    Second,
    Third,
    Fourth,
    Last
}

public class Meetup
{
    private readonly int month, year;

    public Meetup(int month, int year)
    {
        this.month = month;
        this.year = year;
    }

    public DateTime Day(DayOfWeek dayOfWeek, Schedule schedule)
    {
        DateTime dateTime;
        if(schedule == Schedule.Teenth)
        {
            dateTime = new DateTime(year, month, 13);
            while(dateTime.DayOfWeek != dayOfWeek)
            {
                dateTime = dateTime.AddDays(1);
            }
        }
        else if(schedule == Schedule.Last)
        {
            dateTime = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            while (dateTime.DayOfWeek != dayOfWeek)
            {
                dateTime = dateTime.AddDays(-1);
            }
        }
        else
        {
            int occurence = (int)schedule;
            int count = 1;
            dateTime = new DateTime(year, month, 1);
            while (dateTime.DayOfWeek != dayOfWeek)
            {
                dateTime = dateTime.AddDays(1);
            }
            while (count < occurence)
            {
                dateTime = dateTime.AddDays(7);
                count++;
            }
        }

        return dateTime;
    }
}