using System;
using System.Linq;
using System.Numerics;

public class Matrix
{
    int[,] matrix;
    public Matrix(string input)
    {
        int substring = input.IndexOf('\n');
        substring = substring == -1 ? input.Length : substring;
        matrix = new int[input.Count(c => c == '\n')+1, input[..substring].Count(c => c == ' ') + 1];
        string[] rows = input.Split('\n');
        for(int x = 0; x < rows.Length; x++){
            int y = 0;
            foreach(string number in rows[x].Split(' ')){
                matrix[x, y] = int.Parse(number);
                y++;
            }
        }
    }

    public int[] Row(int row) => Enumerable.Range(0, matrix.GetLength(1))
                .Select(x => matrix[row-1, x])
                .ToArray();

    public int[] Column(int col) => Enumerable.Range(0, matrix.GetLength(0))
                .Select(x => matrix[x, col-1])
                .ToArray();
}