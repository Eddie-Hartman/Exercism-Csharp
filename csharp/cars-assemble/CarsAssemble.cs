using System;

static class AssemblyLine
{
    private const int CARS_PER_HOUR = 221;

    public static double ProductionRatePerHour(int speed)
        => speed switch
        {
            0 => 0,
            <5 => speed * CARS_PER_HOUR,
            <9 => speed * CARS_PER_HOUR * .9,
            9 => speed * CARS_PER_HOUR * .8,
            10 => speed * CARS_PER_HOUR * .77,
            _ => throw new ArgumentException("Speed out of range. Must be between 0 - 10."),
        };

    public static int WorkingItemsPerMinute(int speed)
        => (int)(ProductionRatePerHour(speed) / 60);
}
