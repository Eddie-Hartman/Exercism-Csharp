public static class Leap
{
    /// <summary>
    /// Determine if the given year is a leap year.
    /// </summary>
    /// <param name="year">The year to check if it is a leap year.</param>
    /// <returns>Boolean indicating if the year is a leap year.</returns>
    public static bool IsLeapYear(int year)
    {
        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    }
}