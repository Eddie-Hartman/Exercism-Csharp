using System;

class RemoteControlCar
{
    public readonly int speed, batteryDrain;
    public int battery = 100;

    public RemoteControlCar(int speed, int batteryDrain)
    {
        this.speed = speed;
        this.batteryDrain = batteryDrain;
    }

    public bool BatteryDrained() => battery < batteryDrain;

    public int DistanceDriven() => (100 - battery) / batteryDrain * speed;

    public void Drive()
    {
        if(!BatteryDrained())
            battery -= batteryDrain;
    }

    public static RemoteControlCar Nitro() => new(50, 4);
}

class RaceTrack
{
    private readonly int distance;

    public RaceTrack(int distance)
    {
        this.distance = distance;
    }

    public bool CarCanFinish(RemoteControlCar car) => distance <= car.battery / car.batteryDrain * car.speed;
}
