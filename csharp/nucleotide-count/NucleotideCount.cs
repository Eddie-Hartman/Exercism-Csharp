using System;
using System.Collections.Generic;
using System.Linq;

public static class NucleotideCount
{
    private static readonly char[] validNucleotides = new char[] { 'A', 'C', 'G', 'T' };

    public static IDictionary<char, int> Count(string sequence)
    {
        if (sequence.Any(c => !validNucleotides.Contains(c)))
            throw new ArgumentException("Invalid nucleotides.");

        Dictionary<char, int> countDictionary = new Dictionary<char, int>();

        foreach(char c in validNucleotides)
        {
            countDictionary.Add(c, 0);
        }

        foreach(char c in sequence)
        {
            countDictionary[c]++;
        }

        return countDictionary;
    }
}