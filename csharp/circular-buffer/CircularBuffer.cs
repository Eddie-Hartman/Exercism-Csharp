using System;

public class CircularBuffer<T>
{
    private object[] buffer;
    private int writePointer = 0;
    private int readPointer = 0;

    public CircularBuffer(int capacity)
    {
        buffer = new object[capacity];
    }

    public T Read()
    {
        if(buffer[readPointer] is null){
            throw new InvalidOperationException();
        }

        if(buffer[readPointer] is T value)
        {
            buffer[readPointer] = null;
            readPointer = ++readPointer%buffer.Length;
            return value;
        }

        throw new InvalidOperationException();
    }

    public void Write(T value)
    {
        if(buffer[writePointer] is not null){
            throw new InvalidOperationException();
        }
        buffer[writePointer] = value;
        writePointer = ++writePointer%buffer.Length;
    }

    public void Overwrite(T value)
    {
        if(buffer[writePointer] is not null){
            readPointer = ++readPointer%buffer.Length;
        }
        buffer[writePointer] = value;
        writePointer = ++writePointer%buffer.Length;
    }

    public void Clear()
    {
        buffer = new object?[buffer.Length];
    }
}