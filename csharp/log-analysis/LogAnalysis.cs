using System;

public static class LogAnalysis 
{
    public static string SubstringAfter(this string str, string sub) => str[(str.IndexOf(sub) + sub.Length)..];

    public static string SubstringBetween(this string str, string sub1, string sub2) =>
            str[(str.IndexOf(sub1) + sub1.Length)..str.IndexOf(sub2)];

    public static string Message(this string str) => str.SubstringAfter("]: ");

    public static string LogLevel(this string str) => str.SubstringBetween("[", "]");
}