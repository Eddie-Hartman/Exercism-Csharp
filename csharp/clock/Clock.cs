using System;

public class Clock : IEquatable<Clock>
{
    private int hour;
    private int minute;

    public Clock(int hours, int minutes)
    {
        int wrapHours = 0;

        if (minutes < 0)
        {
            wrapHours = (minutes / 60) - (minutes % 60 == 0 ? 0 : 1);
        }
        else
        {
            wrapHours = minutes / 60;
        }

        hour = PositiveRemainder(hours + wrapHours, 24);
        minute = PositiveRemainder(minutes, 60);
    }

    public Clock Add(int minutesToAdd)
    {
        return new(hour, minute + minutesToAdd);
    }

    public Clock Subtract(int minutesToSubtract)
    {
        return Add(0 - minutesToSubtract);
    }

    public override string ToString()
    {
        return $"{hour:00}:{minute:00}";
    }

    public bool Equals(Clock clock)
    {
        return this.ToString() == clock.ToString();
    }

    private int PositiveRemainder(int a, int b)
    {
        return (a % b + b) % b;
    }
}
