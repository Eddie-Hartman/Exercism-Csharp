﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Running;
using System;

namespace Benchmarking
{
    public class Program
    {
        [RPlotExporter]
        [MemoryDiagnoser]
        public class ColorBenchmark
        {
            private static readonly int numberOfStrings = 1000;
            private readonly string[] colors = new string[] { "black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white" };
            public string[] colorStrings = new string[numberOfStrings];
            public ColorBenchmark()
            {
                Random rand = new Random();
                for(int i = 0; i < numberOfStrings; i++)
                {
                    colorStrings[i] = colors[rand.Next(0, 10)];
                }
            }

            [Benchmark]
            public void ColorCode()
            {
                foreach(string color in colorStrings)
                {
                    ResistorColor.ColorCode(color);
                }
            }
        }

        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<ColorBenchmark>();
        }
    }
}
