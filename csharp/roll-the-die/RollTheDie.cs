using System;

public class Player
{
    private Random playerRandom;
    public Random PlayerRandom { get => playerRandom ??= new Random(); private set => playerRandom = value; }

    public int RollDie() => PlayerRandom.Next(1, 18);

    public double GenerateSpellStrength() => Math.Floor(PlayerRandom.NextDouble() * 1000) / 10;
}
