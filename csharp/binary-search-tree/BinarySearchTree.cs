using System.Collections;
using System.Collections.Generic;

public class BinarySearchTree : IEnumerable<int>
{
    public BinarySearchTree(int value) => Value = value;

    public BinarySearchTree(IEnumerable<int> values)
    {
        IEnumerator<int> enumerator = values.GetEnumerator();
        enumerator.MoveNext();
        Value = enumerator.Current;
        while (enumerator.MoveNext())
        {
            Add(enumerator.Current);
        }
    }

    public int Value {get; set;}

    public BinarySearchTree Left {get; set;}

    public BinarySearchTree Right {get; set;}

    public BinarySearchTree Add(int value) => value > Value
            ? Right is not null ? Right.Add(value) : (Right = new BinarySearchTree(value))
            : Left is not null ? Left.Add(value) : (Left = new BinarySearchTree(value));

    public IEnumerator<int> GetEnumerator()
    {
        if (Left != null)
        {
            foreach (int v in Left)
            {
                yield return v;
            }
        }

        yield return Value;

        if (Right != null)
        {
            foreach (int v in Right)
            {
                yield return v;
            }
        }
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}