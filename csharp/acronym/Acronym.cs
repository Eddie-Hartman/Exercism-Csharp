﻿using System;
using System.Text;

public static class Acronym
{
    public static string Abbreviate(string phrase)
    {
        StringBuilder stringBuilder = new StringBuilder();

        foreach(string current in phrase.Split(' ', '-', '_'))
        {
            if(current.Length > 0)
                stringBuilder.Append(char.ToUpper(current[0]));
        }
        return stringBuilder.ToString();
    }
}