using System;

static class SavingsAccount
{
    public static float InterestRate(decimal balance)
        => balance switch
            {
                < 0 => -3.213f,
                < 1000 => .5f,
                < 5000 => 1.621f,
                _ => 2.475f
            };

    public static decimal AnnualBalanceUpdate(decimal balance)
        => balance * (1 + Math.Abs((decimal)InterestRate(balance)) / 100);

    public static int YearsBeforeDesiredBalance(decimal balance, decimal targetBalance)
    {
        int years = 0;
        do
        {
            years++;
            balance = AnnualBalanceUpdate(balance);
        }
        while (balance < targetBalance);
        return years;
    }
}
