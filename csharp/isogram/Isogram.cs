﻿using System;
using System.Linq;

public static class Isogram
{
    public static bool IsIsogram(string word)
    {
        var temp = word.Trim().ToLower().Where(char.IsLetter);
        return temp.Count() == temp.Distinct().Count();
    }
}
