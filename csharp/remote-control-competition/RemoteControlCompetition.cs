using System;
using System.Collections.Generic;

public interface IRemoteControlCar
{
    void Drive();
    int DistanceTravelled { get; }
}

public class ProductionRemoteControlCar : IRemoteControlCar, IComparable
{
    public int DistanceTravelled { get; private set; }
    public int NumberOfVictories { get; set; }

    public void Drive()
    {
        DistanceTravelled += 10;
    }

    public int CompareTo(object obj)
    {
        ProductionRemoteControlCar car = obj as ProductionRemoteControlCar;

        if (this.NumberOfVictories == car.NumberOfVictories)
        {
            return 0;
        }
        else if (this.NumberOfVictories < car.NumberOfVictories)
        {
            return -1;
        }
        return 1;
    }
}

public class ExperimentalRemoteControlCar : IRemoteControlCar
{
    public int DistanceTravelled { get; private set; }

    public void Drive()
    {
        DistanceTravelled += 20;
    }
}

public static class TestTrack
{
    public static void Race(IRemoteControlCar car)
    {
        car.Drive();
    }

    public static List<ProductionRemoteControlCar> GetRankedCars(ProductionRemoteControlCar prc1,
        ProductionRemoteControlCar prc2)
    {
        List<ProductionRemoteControlCar> cars = new List<ProductionRemoteControlCar>(){prc1, prc2};
        cars.Sort();
        return cars;
    }
}
