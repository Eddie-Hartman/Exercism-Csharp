% Base case for empty string
replace([], []).

% Case for replacing G with C
replace([G|T], [C|R]) :-
    G = 'G',
    C = 'C',
    replace(T, R).

% Case for replacing C with G
replace([C|T], [G|R]) :-
    C = 'C',
    G = 'G',
    replace(T, R).

% Case for replacing T with A
replace([T|Rest], [A|R]) :-
    T = 'T',
    A = 'A',
    replace(Rest, R).

% Case for replacing A with U
replace([A|T], [U|R]) :-
    A = 'A',
    U = 'U',
    replace(T, R).

% Fail if any other character is encountered
replace([_|_], _) :- fail.

% Input validation predicate
rna_transcription(S, NewS) :-
    string_chars(S, L),
    (replace(L, R) ->
        string_chars(NewS, R);
    writeln('Invalid string - contains characters other than G, C, T, and A'), fail).
