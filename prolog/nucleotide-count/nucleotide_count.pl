% Define predicate to count a specific character in a list.
count(_, [], 0) :- !.
count(X, [X|T], N) :-
    count(X, T, N2),
    N is N2 + 1, !.
count(X, [Y|T], N) :-
    X \= Y,
    count(X, T, N), !.

% Define predicate to count occurrences of 'A', 'C', 'G', and 'T'.
nucleotide_count(Seq, Counts) :-
    string_chars(Seq, Chars),
    count('A', Chars, A),
    count('C', Chars, C),
    count('G', Chars, G),
    count('T', Chars, T),
    Counts = [('A', A), ('C', C), ('G', G), ('T', T)],
    length(Chars, Len),
    Total is A + C + G + T,
    Total = Len.
