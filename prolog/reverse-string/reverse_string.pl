% base case: an empty list is the reverse of an empty list
reverse_list([], []).

% recursive case: the reverse of a list is the reverse of its tail followed by its head
reverse_list([H|T], Rev) :-
    reverse_list(T, RevT),
    append(RevT, [H], Rev).

% main function: Convert string to reversed string
string_reverse(Input, Reversed) :-
    string_chars(Input, List),
    reverse_list(List, ReverseList),
    string_chars(Reversed, ReverseList).
