isogram(Word) :-
    downcase_atom(Word, LowerWord),
    replace(LowerWord, '-', '', NoHyphenWord),
    replace(NoHyphenWord, ' ', '', NoSpaceWord),
    string_chars(NoSpaceWord, CharList),
    msort(CharList, SortedCharList),
    \+ adjacent_duplicate(SortedCharList).

replace(In, ToReplace, ReplaceWith, Out) :-
    atom_chars(In, InChars),
    delete(InChars, ToReplace, IntermediateChars),
    atom_chars(Intermediate, IntermediateChars),
    delete(IntermediateChars, ReplaceWith, OutChars),
    atom_chars(Out, OutChars).

adjacent_duplicate([X, X|_]).
adjacent_duplicate([_|XS]) :- adjacent_duplicate(XS).
