binary(BinaryString, Number) :-
    atom_chars(BinaryString, BinaryList),
    binary_list_to_number(BinaryList, 0, Number).

binary_list_to_number([], Number, Number).
binary_list_to_number([H|T], Acc, Number) :-
    ((H = '0'; H = '1'), !),
    atom_number(H, HNumber),
    NewAcc is Acc * 2 + HNumber,
    binary_list_to_number(T, NewAcc, Number).
