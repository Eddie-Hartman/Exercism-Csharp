/// <reference path="./global.d.ts" />
// @ts-check

/**
 * Implement the functions needed to solve the exercise here.
 * Do not forget to export them so they are available for the
 * tests. Here an example of the syntax as reminder:
 *
 * export function yourFunction(...) {
 *   ...
 * }
 */

export function cookingStatus(timer){
    if(timer === undefined){
        return "You forgot to set the timer.";
    }
    else if(timer === 0){
        return "Lasagna is done.";
    }
    else{
        return "Not done, please wait.";
    }
}

export function preparationTime(layers, prepTime = 2){
    return layers.length * prepTime;
}

export function quantities(layers){
    var object = { noodles: 0, sauce: 0 };
    layers.forEach((v) => {
        if(v === "noodles"){
            object.noodles += 50;
        }
        else if(v === "sauce"){
            object.sauce += .2;
        }
    });
    return object;
}

export function addSecretIngredient(friendsList, myList){
    myList.push(friendsList[friendsList.length - 1]);
}

export function scaleRecipe(recipe, portions){
    var object = {};
    for (const property in recipe) {
        object[property] = recipe[property] * portions / 2;
    }
    return object;
}