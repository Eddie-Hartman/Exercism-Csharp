local ArmstrongNumbers = {}

function ArmstrongNumbers.is_armstrong_number(number)
    if number == 0 then
        return true
    end

    local length = 0
    local numbers = {}
    local original = number

    while number > 0 do
        local remainder = number % 10
        table.insert(numbers, remainder)
        number = number - remainder
        number = number / 10
        length = length + 1
    end

    local result = 0

    for i = 1, #numbers do
        result = result + (numbers[i] ^ length)
    end

    return result == original
end

return ArmstrongNumbers
