local HighScores = {}
HighScores.__index = HighScores

setmetatable(HighScores, {
    __call = function (cls, ...)
        return cls.new(...)
    end,
})

function HighScores.new(values)
    local self = setmetatable({}, HighScores)
    self.scoresList = values
    return self
end

function HighScores:scores()
    return self.scoresList
end

function HighScores:latest()
    return self.scoresList[#self.scoresList]
end

function HighScores:personal_best()
    local best = 0
    for i = 1, #self.scoresList do
        if best < self.scoresList[i] then
            best = self.scoresList[i]
        end
    end
    return best
end

function HighScores:personal_top_three()
    local best = {}
    local length = 0
    for i = 1, #self.scoresList do
        if length < 3 then
            for j = 1, 3 do
                if best[j] == nil then
                    table.insert(best, self.scoresList[i])
                    break
                elseif best[j] < self.scoresList[i] then
                    table.insert(best, j, self.scoresList[i])
                    break
                end
            end
            length = length + 1
        else
            if best[#best] < self.scoresList[i] then
                local temp = 0
                local temp2 = 0
                for j = 1, 3 do
                    if temp ~= 0 then
                        temp2 = temp
                        temp = best[j]
                        best[j] = temp2
                    elseif best[j] < self.scoresList[i] then
                        temp = best[j]
                        best[j] = self.scoresList[i]
                    end
                end
            end
        end
    end
    return best
end

return HighScores
