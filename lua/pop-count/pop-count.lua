local PopCount = {}

function PopCount.egg_count(number)
    eggs = 0
    while number > 0 do
        remainder = number % 2
        if remainder == 1 then
            eggs = eggs + 1
            number = number - 1
        end
        number = number / 2
    end
    return eggs;
end

return PopCount
