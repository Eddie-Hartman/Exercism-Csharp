local house = {}

local startPhrase = "This is the "
local endPhrase = "house that Jack built."

local nouns = {
    "malt",
    "rat",
    "cat",
    "dog",
    "cow with the crumpled horn",
    "maiden all forlorn",
    "man all tattered and torn",
    "priest all shaven and shorn",
    "rooster that crowed in the morn",
    "farmer sowing his corn",
    "horse and the hound and the horn"
}

local actions = {
    "lay in",
    "ate",
    "killed",
    "worried",
    "tossed",
    "milked",
    "kissed",
    "married",
    "woke",
    "kept",
    "belonged to"
}

house.verse = function(which)
    local value = startPhrase
    for j = which - 1, 1, -1 do
        value = value .. nouns[j] .. "\nthat " .. actions[j] .. " the "
    end
    value = value .. endPhrase
    return value
end

house.recite = function()
    local value = ""
    for i = 1, 12 do
        value = value .. house.verse(i)
        if i ~= 12 then
            value = value .. "\n"
        end
    end
    return value
end

return house
