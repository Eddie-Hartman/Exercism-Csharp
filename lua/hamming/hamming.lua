local Hamming = {}

function Hamming.compute(a,b)
    if string.len(a) ~= string.len(b) then
        return -1
    end

    distance = 0;

    for i = 1, string.len(a) do
        char1 = string.sub(a, i, i)
        char2 = string.sub(b, i, i)
        if char1 ~= char2 then
            distance = distance + 1
        end
    end
    return distance
end

return Hamming
