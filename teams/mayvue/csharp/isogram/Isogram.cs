﻿using System;

public static class Isogram
{
    public static bool IsIsogram(string word)
    {
        word = word.ToLower();
        char[] letters = word.ToCharArray();
        bool[] letterBools = new bool[26];

        foreach(char letter in letters)
        {
            int num = (int)letter;
            if (num < 'a' || num > 'z')
                continue;
            num %= 'a';
            if (letterBools[num])
                return false;
            letterBools[num] = true;
        }
        return true;
    }
}
