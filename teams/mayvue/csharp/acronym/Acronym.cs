﻿using System;
using System.Text;

public static class Acronym
{
    public static string Abbreviate(string phrase)
    {
        StringBuilder stringBuilder = new StringBuilder();
        string[] strings = phrase.ToUpper().Split(' ', '-', '_');

        foreach(string current in strings)
        {
            if(current.Length > 0)
                stringBuilder.Append(current[0]);
        }
        return stringBuilder.ToString();
    }
}