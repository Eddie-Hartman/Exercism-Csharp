﻿using System;

public static class ResistorColor
{
    private static readonly string[] colors = new string[] { "black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white" };

    public static string[] Colors() => colors;

    public static int ColorCode(string color)
    {
        color = color.ToLower();
        return Array.IndexOf(colors, color);
    }
}