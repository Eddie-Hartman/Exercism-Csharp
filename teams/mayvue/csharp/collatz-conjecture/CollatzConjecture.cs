﻿using System;

public static class CollatzConjecture
{
    public static int Steps(int number) => number < 1 ? throw new ArgumentOutOfRangeException("Number must be 1 or more.") :
        number == 1 ? 0 :
        1 + (number % 2 == 0 ? Steps(number / 2) :
        Steps(number * 3 + 1));
}