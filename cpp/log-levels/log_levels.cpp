#include <string>

namespace log_line {
    int index(std::string log){
        return log.find("]: ");
    }

    std::string message(std::string log){
        return log.substr(index(log) + 3);
    }

    std::string log_level(std::string log){
        return log.substr(1, index(log) - 1);
    }

    std::string reformat(std::string log){
        return message(log) + " (" + log_level(log) + ")";
    }
} // namespace log_line
