#!/usr/bin/env bash

if [ "$#" -ne 2 ]
    then echo "Usage: hamming.sh <string1> <string2>"
    exit 1
fi

if [ ${#1} -ne ${#2} ]
    then echo "strands must be of equal length"
    exit 1
fi

count=0

for (( i=0; i<${#1}; i++ ))
do
    char1=${1:$i:1}
    char2=${2:$i:1}
    
    if [ "$char1" != "$char2" ]; then
        ((count++))
    fi
done

echo "$count"