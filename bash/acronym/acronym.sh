#!/usr/bin/env bash

result=""
newWord=true

for (( i=0; i<${#1}; i++ ))
do
    char=${1:$i:1}
    
    if [[ $char =~ [a-zA-Z\'] ]]
        then if $newWord
            then result+=$(echo "$char" | tr 'a-z' 'A-Z')
            newWord=false
        fi
        else newWord=true
    fi
done

echo "$result"